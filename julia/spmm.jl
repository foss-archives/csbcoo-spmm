using Printf
using ArgParse

function readCSBCOO(fileName::String)
    f = open(fileName)

    dimension = read(f, Int64)
    tile_size = read(f, Int32)

    A = Dict{Tuple{Int32, Int32}, Tuple{Array{Int16,1}, Array{Int16,1}, Array{Float32,1}}}()
    A_diagonal = Dict{Tuple{Int32, Int32}, Tuple{Array{Int16,1}, Array{Int16,1}, Array{Float32,1}}}()

    nnz = 0
    nnz_diagonal = 0

    while(!eof(f))
        p = read(f, Int32)
        q = read(f, Int32)
        tile_nnz = read(f, Int32)
        if p == q
            nnz_diagonal = nnz_diagonal + tile_nnz
        else
            nnz = nnz + tile_nnz
        end

        i = Array{Int16}(undef, tile_nnz)
        j = Array{Int16}(undef, tile_nnz)
        v = Array{Float32}(undef, tile_nnz)

        read!(f, i)
        read!(f, j)
        read!(f, v)

        i = i .+ one(Int16)
        j = j .+ one(Int16)

        if p == q
            A_diagonal[p,q] = (i,j,v)
        else
            A[p,q] = (i,j,v)
        end
    end
    return A, A_diagonal, dimension, tile_size, nnz, nnz_diagonal
end

function SPMM_diag(A, n, k, nnz, m)
    X = ones(Float32, m, n)
    Y = ones(Float32, m, n)
    ntiles = n / k

    t = @elapsed for ((p,q), tile) in A
        # tile contains COO matrix elements (I,J,V)
        for (i,j,v) in zip(tile[1], tile[2], tile[3])
            ig = i + p*k
            jg = j + p*k
            for b in 1:m
                Y[b, ig] += v * X[b, ig]
            end
        end
    end

    flops = 2 * m * nnz
    mflops = flops / t / 1e6
    return mflops
end

function SPMM(A, n, k, nnz, m)
    X = ones(Float32, m, n);
    Y = ones(Float32, m, n);

    t = @elapsed for ((p,q), tile) in A
        # tile contains COO matrix elements (I,J,V)
        for (i,j,v) in zip(tile[1], tile[2], tile[3])
            ig = i + p*k
            jg = j + q*k
            for b in 1:m
                Y[b, ig] += v * X[b, ig]
                Y[b, jg] += v * X[b, jg]
            end
        end
    end

    flops = 4 * m * nnz
    mflops = flops / t / 1e6
    return mflops
end

function parse_cli()

    s = ArgParseSettings(description = "CSB_Coo SPMM benchmark")

    @add_arg_table! s begin
        "-m"
            help = "an option with an argument"
            arg_type = Int
            default = 1
        "--ntrials"
            help = "number of iterations, reports max()"
            arg_type = Int
            default = 2
        "filename"
            help = "CSB COO format input file"
            required = true
    end

    return parse_args(s)
end

function main()
    args = parse_cli()
    ntrials = args["ntrials"]
    m = args["m"]
    filename = args["filename"]
    A, A_diagonal, n, k, nnz, nnz_diagonal = readCSBCOO(filename)
    mflops = maximum([SPMM_diag(A_diagonal, n, k, nnz_diagonal, m) for trial in 1:ntrials])
    @printf("diagonal: m = %i mflops = %f\n", m, mflops)
    mflops = maximum([SPMM(A, n, k, nnz, m) for trial in 1:ntrials])
    @printf("m = %i mflops = %f\n", m, mflops)
end

main()