#include "hash.hpp" // hash specialization for tuple
#include "timer.hpp"
#include <fstream>
#include <getopt.h>
#include <iostream>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>


struct Matrix {
  long long n; // dimension
  int k;       // tile size
  long long nnz = 0;
  long *A_ptr;
  long *A_nnz;
  std::vector<short> A_i, A_j;
  std::vector<float> A_value;  
  Matrix(long long n, int k) : n(n), k(k){};
};

Matrix read_input(std::string inputfile) {
  std::ifstream ifs(inputfile, std::ios::in | std::ios::binary);

  long long n; // matrix dimension
  int k;       // tile size

  if (!ifs.is_open())
    std::exit(2);

  // read header
  ifs.read(reinterpret_cast<char *>(&n), sizeof(n));
  ifs.read(reinterpret_cast<char *>(&k), sizeof(k));

  Matrix matrix(n, k);

  long ntiles = n / k;
  
  matrix.A_ptr = new long[ntiles*ntiles];
  matrix.A_nnz = new long[ntiles*ntiles];
  for (long i=0; i<ntiles*ntiles; i++) {
    matrix.A_nnz[i] = 0;
    matrix.A_ptr[i] = 0;
  }

  matrix.nnz = 0;
  
  do { // loop over tiles
    // header
    int p, q, tile_nnz;
    ifs.read(reinterpret_cast<char *>(&p), sizeof(p));
    ifs.read(reinterpret_cast<char *>(&q), sizeof(q));
    ifs.read(reinterpret_cast<char *>(&tile_nnz), sizeof(tile_nnz));

    matrix.A_nnz[p*ntiles + q] = tile_nnz;
    matrix.A_ptr[p*ntiles + q] = matrix.nnz;
    matrix.nnz += tile_nnz;
    
    // data
    std::vector<short> i(tile_nnz), j(tile_nnz);
    std::vector<float> v(tile_nnz);
    ifs.read(reinterpret_cast<char *>(&i[0]), tile_nnz * sizeof(i[0]));
    ifs.read(reinterpret_cast<char *>(&j[0]), tile_nnz * sizeof(j[0]));
    ifs.read(reinterpret_cast<char *>(&v[0]), tile_nnz * sizeof(v[0]));

    matrix.A_i.insert(matrix.A_i.end(), i.begin(), i.end());
    matrix.A_j.insert(matrix.A_j.end(), j.begin(), j.end());
    matrix.A_value.insert(matrix.A_value.end(), v.begin(), v.end());
    
  } while (ifs.peek() != std::ifstream::traits_type::eof());

  return matrix;
}

void usage(char *argv[]) {
  std::cout << "usage: " << argv[0] << " [-h] [-m BLOCKSIZE] <inputfile>\n\n"
            << "Benchmark SPMM performance\n\n"
            << "Required arguments:\n"
            << "  <inputfile>  matrix in CSBCOO binary format\n\n"
            << "Optional arguments:\n"
            << "  -h           show this message and exit\n"
            << "  -m BLOCKSIZE blocksize for vector (default: 1 (spmv))\n"
            << std::endl;
}

int main(int argc, char *argv[]) {
  
  int m = 1;
  std::string inputfile;

  int iarg = 0;
  while ((iarg = getopt(argc, argv, "m:h")) != -1) {
    switch (iarg) {
    case 'm':
      m = std::atoi(optarg);
      break;
    case 'h':
      usage(argv);
      std::exit(1);
    }
  }

  if (optind == argc - 1) {
    inputfile = std::string(argv[optind]);
  } else {
    usage(argv);
    std::exit(1);
  }

  Timer t;
  t.start("Read Matrix");
  auto A = read_input(inputfile);
  t.stop("Read Matrix");

  int tile_size = A.k;
  short *A_i = A.A_i.data();
  short *A_j = A.A_j.data();
  float *A_value = A.A_value.data();
  long *A_nnz = A.A_nnz;
  long *A_ptr = A.A_ptr;
  
  float *X = (float*) malloc(A.n*m*sizeof(float));
  float *Y = (float*) malloc(A.n*m*sizeof(float)); 

  for (long i=0; i<A.n*m; i++) {
    X[i] = 1.0;
    Y[i] = 2.0;
  }

  long ntiles = A.n / A.k;
  long long flops = 0;
  long long flops_diagonal = 0;
  for (long i=0; i<ntiles; i++) {
    flops_diagonal += 2*m*A_nnz[i*ntiles + i];
    for (long j=0; j<i; j++) {
      flops += 4*m*A_nnz[i*ntiles + j];
    }
  }
      
  t.start("SPMM_diag");  
  for (long i=0; i<ntiles; i++) {
    auto ind = i*ntiles + i;
    auto nnz_tile = A_nnz[ind];
    if (nnz_tile > 0) {
      auto tile_loc = A_ptr[ind];
      for (int k=0; k<nnz_tile; k++) {
	auto r = i*tile_size + A_i[tile_loc + k];
	auto c = i*tile_size + A_j[tile_loc + k];
	auto value = A_value[tile_loc + k];
	for (int b=0; b<m; b++) {
	  Y[r+b] += value * X[c+b];
	}
      }
    }
  }
  t.stop("SPMM_diag", flops_diagonal);
    
  t.start("SPMM");
  for (long i=0; i<ntiles; i++) {
    for (long j=0; j<i; j++) {      
      auto ind = i*ntiles + j;
      auto nnz_tile = A_nnz[ind];
      if (nnz_tile > 0) {
	auto tile_loc = A_ptr[ind];
	for (int k=0; k<nnz_tile; k++) {
	  auto r = i*tile_size + A_i[tile_loc + k];
	  auto c = i*tile_size + A_j[tile_loc + k];
	  auto value = A_value[tile_loc + k];
	  for (int b=0; b<m; b++) {
	    Y[r+b] += value * X[c+b];
	    Y[c+b] += value * X[r+b];
	  }
	}
      }
    }
  }
  t.stop("SPMM", flops);
  
  t.report();
  return 0;
}
