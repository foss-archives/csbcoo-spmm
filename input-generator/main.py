#!/usr/bin/env python3
import numpy as np
import scipy.sparse as sp
from tqdm import tqdm
import argparse
import struct


def write_tile(f, dim, maxcount, block_coords, nnz=None):
    """ TODO: CHECK DENSITY """
    if nnz is None:
        maxdensity = float(maxcount) / (dim*dim)
        density = maxdensity * np.random.random_sample()
    else:
        density = float(nnz) / (dim*dim)

    m = sp.random(dim, dim, density, format='coo')

    if m.nnz != 0:
        f.write(struct.pack('iii', *block_coords, m.nnz))
        f.write(m.row.astype(np.int16))
        f.write(m.col.astype(np.int16))
        f.write(m.data.astype(np.float32))

    return m.nnz


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Create a sparse CSB COO matrix')
    parser.add_argument('-n', '--dimension', type=int, default=4096,
                        help='matrix dimension')
    parser.add_argument('-k', '--tilesize', type=int, default=512,
                        help='tile size')
    parser.add_argument('-z', '--nnz', type=int, default=42,
                        help='number of nonzeros')
    parser.add_argument('-r', '--maxtilerho', type=float, default=0.1,
                        help='max density of tiles')
    parser.add_argument('outputfile', type=str,
                        help='name of output file')

    args = parser.parse_args()

    dimension = args.dimension
    tilesize = args.tilesize
    nnz = args.nnz
    maxtilennz = int(tilesize*tilesize*args.maxtilerho)

    assert dimension % tilesize == 0

    nt = dimension / tilesize
    # lower triangular indices (H in MFDn is symmetric)
    ib, jb = np.tril_indices(nt)
    coords = list(zip(ib, jb))

    # shuffled indicies
    rng = np.random.default_rng()
    ntilesmax = len(ib)
    ind = np.arange(ntilesmax)
    rng.shuffle(ind)

    with open(args.outputfile, 'wb') as f:
        f.write(struct.pack('qi', dimension, tilesize))
        t = tqdm(total=nnz)
        nnzcount = 0
        i = 0
        while(nnzcount < nnz - maxtilennz and i < ntilesmax - 1):
            ij = coords[ind[i]]
            nnztile = write_tile(f, tilesize, maxtilennz, ij)
            t.update(nnztile)
            if nnztile != 0:
                i += 1
            nnzcount += nnztile

        # last tile to get requested nonzeros
        if (nnzcount < nnz):
            ij = coords[ind[i]]
            nnztile = write_tile(f, tilesize, maxtilennz, ij, nnz-nnzcount)
            t.update(nnztile)
            if nnztile != 0:
                i += 1
            nnzcount += nnztile

        t.close()

    print(f'wrote nnz={nnzcount} elements in {i} tiles to {args.outputfile}')
