# CSB COO matrix generator

## Setup

```
pip install -r requirements.txt
```

or

```
conda env create -f env.yml
```

## Usage

```console
$ ./main.py -n 524288 -k 512 -z 268435456 -r 0.1 524288.512.268435456.bin
100%|████████████████████████████████████████████| 268435456/268435456 [01:07<00:00, 4004096.66it/s]
wrote nnz=268435456 elements in 20550 tiles to 524288.512.268435456.bin
```
